import Login from "./components/Login.vue";
import MainDashboard from "./components/Dashboard/Dashboard.vue";
import Imports from "./components/Imports/Imports.vue";
import Exports from "./components/Exports/Exports.vue";
import ProductDashboard from "./components/Products/ProductDashboard.vue";
import ProductPage from "./components/Products/ProductPage.vue"; 
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  { path: "/", component: Login },
  { path: "/MainDashboard", component: MainDashboard },
  { path: "/Imports", component: Imports },
  { path: "/Exports", component: Exports },
  { path: "/Products", component: ProductDashboard },
  { path: "/Products/:product", component: ProductPage },


];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
