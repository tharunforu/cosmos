import { createApp } from 'vue'
import App from './App.vue'
import router from './routes.js'
import PrimeVue from 'primevue/config';

//theme
import "primevue/resources/themes/bootstrap4-light-blue/theme.css";
import 'primeicons/primeicons.css';

const app = createApp(App);

app.use(PrimeVue);
app.use(router);

app.mount('#app');
