const express  = require('express');
const app = express();
const SETTINGS = require("./config/config");
app.get("/",(req,res)=>{
    res.send("hello cosmosians");
})
app.listen(SETTINGS.PORT,(req,res)=>{
    console.log("listening on port:"+SETTINGS.PORT);
})